import '../../node_modules/jquery/src/jquery';
// import '../../node_modules/nouislider/distribute/nouislider'; // console error - need fix !!!


$(document).ready(function() {

  // Add Language Drop List
  let languageButton = $('.language-list__button'),
    languageItem = $('.language-list__item'),
    languageContainer = $('.language-list'),
    languageDropList = $('.language-list__drop-list');

  function languageFunction() { // Need fix this code !!! Bag no correct work !!!
    if ($(window).width() > 991) {
      languageButton.click( function(e) {
        e.preventDefault();
        languageDropList.show();
      });
      $(document).mouseup(function(e) {
        if (languageContainer.has(e.target).length === 0) {
          languageDropList.hide();
        }
      });
      languageItem.click(function() {
        let itemText = $(this).html();
        languageDropList.hide();
        if ($(this).hasClass('language-list__item_active') === false) {
          languageItem.removeClass('language-list__item_active');
          $(this).addClass('language-list__item_active');
        } else {
          $(this).addClass('language-list__item_active');
        }
        languageButton.html(itemText);
      });
    } else {
      languageDropList.show();
    }
  }
  languageFunction();
  $(window).resize(function() {
    if ($(window).width() > 991) {
      languageFunction();
    } else {
      languageDropList.show();
    }
  });


  // Add Yacht Name Drop List

  let yachtButton = $('.yacht-name__button'),
    yachtItem = $('.yacht-name__item'),
    yachtContainer = $('.yacht-name'),
    yachtDropList = $('.yacht-name__drop-list');

  yachtButton.click( function(e) {
    e.preventDefault();
    yachtDropList.fadeToggle();
  });
  $(document).mouseup(function(e) {
    if (yachtContainer.has(e.target).length === 0) {
      yachtDropList.hide();
    }
  });

  yachtItem.click(function() {
    let itemText = $(this).html();
    yachtDropList.fadeToggle();
    if ($(this).hasClass('language-list__item_active') === false) {
      yachtItem.removeClass('language-list__item_active');
      $(this).addClass('language-list__item_active');
    } else {
      $(this).addClass('language-list__item_active');
    }
    yachtButton.html(itemText);
  });



  // MENU BURGER

  $('.header__button').click(function() { // Need optimization code
    $('.header__button').toggleClass('header__button_active');
    $('.header__burger-menu').toggleClass('header__burger-menu_active');
    $('.header__burger-bar').toggleClass('header__burger-bar_active');

  });

  // Yacht Name Burger Drop Menu

  $('.mobile-search__button_yacht-name').click(function(e) { // - Need OPTIMIZATION this code
    e.preventDefault();
    $('.mobile-search__yacht-name').toggleClass('mobile-search__yacht-name_active');
  });
  $('.mobile-search__yacht-name_close').click(function(e) {
    e.preventDefault();
    $('.mobile-search__yacht-name').removeClass('mobile-search__yacht-name_active');
  });
  $('.mobile-search__button_search').click(function(e) {
    e.preventDefault();
    $('.mobile-search__form').toggleClass('mobile-search__form_active');
  });
  $('.mobile-search__yacht-name_close').click(function(e) {
    e.preventDefault();
    $('.mobile-search__form').removeClass('mobile-search__form_active');
  });

  // Add noUiSlider


  var snapSlider = document.getElementById('slider-snap');

  noUiSlider.create(snapSlider, {
    start: [0, 200],
    connect: true,
    step: 10,
    range: {
      'min': 0,
      'max': 200
    },
    format: {
      to: function(value) {
        return '$ ' + value + ' M'; // Need add ability to change currency
      },
      from: function(value) {
        return value.replace('', '');
      }
    }
  });
  var snapValues = [
    document.getElementById('slider-snap-value-lower'),
    document.getElementById('slider-snap-value-upper')
  ];

  snapSlider.noUiSlider.on('update', function(values, handle) {
    snapValues[handle].innerHTML = values[handle];
  });
});
